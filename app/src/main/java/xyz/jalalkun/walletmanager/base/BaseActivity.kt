package xyz.jalalkun.walletmanager.base

import android.content.res.Configuration
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.analytics.FirebaseAnalytics
import org.jetbrains.anko.configuration
import xyz.jalalkun.walletmanager.util.Cons.SIMPAN_TEMA
import xyz.jalalkun.walletmanager.util.LogTest
import xyz.jalalkun.walletmanager.util.Sessions

open class BaseActivity : AppCompatActivity() {
    private val TAG = "BaseActivity"
    lateinit var sessions: Sessions
    private var tema = 0
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        sessions = Sessions.getInstance(this)!!
        viewReSet()
    }

    fun viewReSet(){
        tema = sessions.getIntValue(SIMPAN_TEMA)
        LogTest().e(TAG, "tema $tema")
        when(tema){
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM->AppCompatDelegate.setDefaultNightMode(tema)
            AppCompatDelegate.MODE_NIGHT_NO->AppCompatDelegate.setDefaultNightMode(tema)
            AppCompatDelegate.MODE_NIGHT_YES->AppCompatDelegate.setDefaultNightMode(tema)
            0->{
                sessions.setIntValue(SIMPAN_TEMA, AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                viewReSet()
            }
        }

        when (configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_NO -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val window = window
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    window.statusBarColor = Color.parseColor("#000000")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        window.statusBarColor = Color.parseColor("#FFFFFF")
                        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    }
                }
            } // Night mode is not active, we're using the light theme
            Configuration.UI_MODE_NIGHT_YES -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val window = window
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    window.statusBarColor = Color.parseColor("#000000")
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//                    }
                }
            } // Night mode is active, we're using dark theme
        }

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }
}