package xyz.jalalkun.walletmanager.menu

import android.os.Bundle
import android.view.LayoutInflater
import org.jetbrains.anko.sdk27.coroutines.onClick
import xyz.jalalkun.walletmanager.BuildConfig
import xyz.jalalkun.walletmanager.base.BaseActivity
import xyz.jalalkun.walletmanager.databinding.ActivityAboutBinding

class AboutActivity : BaseActivity() {
    private lateinit var binding: ActivityAboutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAboutBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        binding.back.onClick {
            finish()
        }
        binding.version.text = ("Versi ${BuildConfig.VERSION_NAME}")
    }
}