package xyz.jalalkun.walletmanager.menu

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import org.jetbrains.anko.image
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast
import xyz.jalalkun.walletmanager.R
import xyz.jalalkun.walletmanager.base.BaseActivity
import xyz.jalalkun.walletmanager.databinding.ActivityCategoryBinding
import xyz.jalalkun.walletmanager.databinding.AdapterListCategoryBinding
import xyz.jalalkun.walletmanager.databinding.PopupAddCategoryBinding
import xyz.jalalkun.walletmanager.db.category.Category
import xyz.jalalkun.walletmanager.db.category.CategoryiewModel
import xyz.jalalkun.walletmanager.db.log.LogKeuanganViewmodel
import xyz.jalalkun.walletmanager.util.Cons
import xyz.jalalkun.walletmanager.util.Cons.IN
import xyz.jalalkun.walletmanager.util.Cons.OUT
import xyz.jalalkun.walletmanager.util.LogTest
import xyz.jalalkun.walletmanager.util.SwipeToDeleteCallback
import xyz.jalalkun.walletmanager.util.SwipeToEditCallBack
import java.util.*

class CategoryActivity : BaseActivity() {
    private val tag = "CategoryActivity"
    private lateinit var binding: ActivityCategoryBinding
    private lateinit var categoryiewModel: CategoryiewModel
    private lateinit var logKeuanganViewmodel: LogKeuanganViewmodel
    private var types = emptyList<Category>()
    private var allCategory = emptyList<Category>()
    private var checkId = 0
    private var y = 0
    private var last_id_category = ""
    private var total = 0
    private lateinit var categorySwipte: Category
    private lateinit var adapterListCategory: AdapterListCategory
    private var isSwipe = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCategoryBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        categoryiewModel = run {
            ViewModelProvider(this)[CategoryiewModel::class.java]
        }

        logKeuanganViewmodel = run {
            ViewModelProvider(this)[LogKeuanganViewmodel::class.java]
        }

        /**
         * live data all category
         * */
        categoryiewModel.allCategory.observe(this, Observer {
            /**
             * mendapatkan id terakhir
             * */
            allCategory = it
            val lastCategory = it.last()
            last_id_category = lastCategory.id
        })

        binding.toggleButton.isSingleSelection = true
        binding.toggleButton.addOnButtonCheckedListener { _, checkedId, isChecked ->
            LogTest().e(tag, " $checkedId $isChecked")
            if (isChecked) {
                checkId = checkedId
                when (checkedId) {
                    binding.togglePemasukan.id -> {
                        if (!isChecked) binding.togglePemasukan.isChecked = true
                        binding.togglePengeluaran.isChecked = false
                        filterType(Cons.IN)
                        LogTest().e(tag, "pemasukan")
                    }
                    binding.togglePengeluaran.id -> {
                        if (!isChecked) binding.togglePengeluaran.isChecked = true
                        binding.togglePemasukan.isChecked = false
                        filterType(Cons.OUT)
                        LogTest().e(tag, "pengeluaran")
                    }
                }
            } else {
                if (checkId == checkedId) binding.toggleButton.check(checkId)
                when (checkedId) {
                    binding.togglePemasukan.id -> LogTest().e(tag, "unCheck pemasukan")
                    binding.togglePengeluaran.id -> LogTest().e(tag, "uncheck pengeluaran")
                }
            }
        }

        /**
         * set adapter to recycler view
         * */
        adapterListCategory = AdapterListCategory(this)
        binding.rvCategory.layoutManager = LinearLayoutManager(this)
        binding.rvCategory.adapter = adapterListCategory
        binding.rvCategory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > y) {
                    binding.fabLogKeuangan.show()
                    y = dy
                }
                if (dy < y) {
                    binding.fabLogKeuangan.hide()
                    y = dy
                }
            }
        })

        /**
         * get total size untuk kategory yang diswipe
         * */
        logKeuanganViewmodel.filterCategory.observe(this, Observer {
            LogTest().e("AdapterListCategory", Gson().toJson(it))
            if (isSwipe) {
                isSwipe = false
                if (it.isEmpty()) return@Observer
                else if (it[0].type == categorySwipte.id) {
                    total = it.size
                }
            }
        })

        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                isSwipe = true
                val category = types[viewHolder.adapterPosition]
                categorySwipte = category
                if (category.isDefault) {
                    toast(getString(R.string.default_cant_delete))
                    adapterListCategory.setData(types)
                } else {
                    val alertDelete = AlertDialog.Builder(this@CategoryActivity).create()
                    alertDelete.setTitle("Delete ${category.nama}")
                    alertDelete.setMessage("${this@CategoryActivity.getString(R.string.really_delete)} ${category.nama}?")
                    alertDelete.setButton(AlertDialog.BUTTON_POSITIVE, "Yes") { dialog, _ ->
                        dialog.dismiss()
                        when {
                            category.isDefault -> {
                                val dialogDefault =
                                    AlertDialog.Builder(this@CategoryActivity).create()
                                dialogDefault.setMessage(this@CategoryActivity.getString(R.string.default_cant_delete))
                                dialogDefault.setButton(
                                    AlertDialog.BUTTON_NEUTRAL,
                                    "Ok"
                                ) { dial, _ ->
                                    dial.dismiss()
                                }
                                dialogDefault.show()
                            }
                            total == 0 -> categoryiewModel.delete(category)
                            else -> this@CategoryActivity.toast(getString(R.string.cant_delete_category_with_data))
                        }
                    }
                    alertDelete.setButton(AlertDialog.BUTTON_NEGATIVE, "No") { dialog, _ ->
                        adapterListCategory.setData(types)
                        dialog.dismiss()
                    }
                    alertDelete.show()
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(binding.rvCategory)
        val swipeEdit = object : SwipeToEditCallBack(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                isSwipe = true
                categorySwipte = types[viewHolder.adapterPosition]
                popupEditCategory(types[viewHolder.adapterPosition])
            }
        }
        val editTouchHelper = ItemTouchHelper(swipeEdit)
        editTouchHelper.attachToRecyclerView(binding.rvCategory)

        categoryiewModel.filterType.observe(this, Observer {
            types = it
            adapterListCategory.setData(it)
        })
        binding.togglePemasukan.isChecked = true
        setClick()
    }

    fun setClick() {
        binding.fabLogKeuangan.onClick { popupAddCategory() }
        binding.back.onClick { finish() }
    }

    private fun filterType(jenis: Int) {
        categoryiewModel.getFilterType(this, jenis)
    }

    private fun popupEditCategory(category: Category) {
        if (category.isDefault) {
            toast(getString(R.string.default_cant_edit))
            adapterListCategory.setData(types)
        } else {
            val bindingPopUpEdit = PopupAddCategoryBinding.inflate(LayoutInflater.from(this))
            val alertBuilder = AlertDialog.Builder(this)
            alertBuilder.setView(bindingPopUpEdit.root)
            val alertDialog = alertBuilder.create()
            when (category.inOut) {
                IN -> bindingPopUpEdit.rbIncome.isChecked = true
                OUT -> bindingPopUpEdit.rbSpending.isChecked = true
            }
            alertDialog.setOnDismissListener {
                adapterListCategory.setData(types)
            }
            bindingPopUpEdit.etCategoryName.setText(category.nama)
            bindingPopUpEdit.btnBatal.onClick { alertDialog.dismiss() }
            bindingPopUpEdit.btnSimpan.onClick {
                if (bindingPopUpEdit.etCategoryName.text!!.isEmpty()) {
                    toast(getString(R.string.fill_category_name))
                } else {
                    val id = category.id
                    var inOut = OUT
                    if (bindingPopUpEdit.rgCategoryType.checkedRadioButtonId == bindingPopUpEdit.rbIncome.id) inOut =
                        IN
                    val categoryEdit = Category(
                        id = id,
                        nama = bindingPopUpEdit.etCategoryName.text!!.toString()
                            .toUpperCase(Locale.ROOT),
                        inOut = inOut,
                        warna = "FFFFFF",
                        icon = "",
                        isDefault = category.isDefault
                    )
                    categoryiewModel.update(categoryEdit)
                    categoryiewModel.resultInsert.observe(this@CategoryActivity, Observer {
                        LogTest().e(tag, "result update $it")
                    })
                    alertDialog.dismiss()

                }
            }
            alertDialog.show()
        }
    }

    private fun popupAddCategory() {
        val bindingPopUpAdd = PopupAddCategoryBinding.inflate(LayoutInflater.from(this))
        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setView(bindingPopUpAdd.root)
        val alertDialog = alertBuilder.create()
        bindingPopUpAdd.rbIncome.isChecked = true
        bindingPopUpAdd.btnBatal.onClick { alertDialog.dismiss() }
        bindingPopUpAdd.btnSimpan.onClick {
            if (bindingPopUpAdd.etCategoryName.text!!.isEmpty()) {
                toast(getString(R.string.fill_category_name))
            } else {
                if (nameIsExist(bindingPopUpAdd.etCategoryName.text.toString())) {
                    toast(getString(R.string.name_already_exist))
                    return@onClick
                } else {
                    var id = last_id_category.toInt()
                    id += 1
                    var in_out = OUT
                    if (bindingPopUpAdd.rgCategoryType.checkedRadioButtonId == bindingPopUpAdd.rbIncome.id) in_out =
                        IN
                    val categoryAdd = Category(
                        id = id.toString(),
                        nama = bindingPopUpAdd.etCategoryName.text!!.toString()
                            .toUpperCase(Locale.ROOT),
                        inOut = in_out,
                        warna = "FFFFFF",
                        icon = "",
                        isDefault = false
                    )
                    categoryiewModel.insert(categoryAdd)
                    categoryiewModel.resultInsert.observe(this@CategoryActivity, Observer {
                        LogTest().e(tag, "result insert $it")
                    })
                    alertDialog.dismiss()
                }
            }
        }
        alertDialog.show()

    }

    private fun nameIsExist(nama: String): Boolean {
        var bol = false
        var name = ""
        name = nama.toUpperCase(Locale.ROOT)
        for (cat in allCategory) {
            LogTest().e(tag, "cat nama ${cat.nama} nama $name")
            if (cat.nama == name) bol = true
        }
        LogTest().e(tag, "name exist $bol")
        return bol
    }

    class AdapterListCategory(val activity: CategoryActivity) :
        RecyclerView.Adapter<AdapterListCategory.Holder>() {
        private var data = emptyList<Category>()
        private lateinit var binding: AdapterListCategoryBinding
        private lateinit var categorViewmodel: CategoryiewModel
        private lateinit var logKeuanganViewmodel: LogKeuanganViewmodel

        class Holder(itemView: AdapterListCategoryBinding) :
            RecyclerView.ViewHolder(itemView.root) {
            val no = itemView.nomer
            val nama = itemView.namaCategory
            val ivUpDown = itemView.ivUpDown
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            binding = AdapterListCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            categorViewmodel =
                parent.context.run { ViewModelProvider(activity)[CategoryiewModel::class.java] }
            logKeuanganViewmodel =
                parent.context.run { ViewModelProvider(activity)[LogKeuanganViewmodel::class.java] }
            return Holder(binding)
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(h: Holder, position: Int) {
            val category = data[position]
            logKeuanganViewmodel.filterCategory.observe(activity, Observer {
                LogTest().e("AdapterListCategory", Gson().toJson(it))
                if (it.isEmpty()) return@Observer
                else if (it[0].type == category.id) {
                    LogTest().e("AdapterListCategory", "size ${it.size}")
                    h.no.text = ("${it.size}")
                }
            })
            logKeuanganViewmodel.getByCategory(activity, category.id)
            h.nama.text = category.nama
            if (category.inOut == IN) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    h.ivUpDown.image =
                        h.itemView.context.getDrawable(R.drawable.ic_up_green)
                } else {
                    h.ivUpDown.image =
                        h.itemView.resources.getDrawable(R.drawable.ic_up_green)
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    h.ivUpDown.image =
                        h.itemView.context.getDrawable(R.drawable.ic_down_red)
                } else {
                    h.ivUpDown.image =
                        h.itemView.resources.getDrawable(R.drawable.ic_down_red)
                }
            }
//            h.itemView.onLongClick {
//                val popupEditDelete = PopupEditDeleteKategoriBinding.inflate(LayoutInflater.from(activity), null, false)
//                val builder = AlertDialog.Builder(h.itemView.context)
//                builder.setView(popupEditDelete.root)
//                val alert = builder.create()
//                popupEditDelete.deleteKategory.onClick {
//                    alert.dismiss()
//                    val alertDelete = AlertDialog.Builder(h.itemView.context).create()
//                    alertDelete.setTitle("Delete ${category.nama}")
//                    alertDelete.setMessage("${h.itemView.context.getString(R.string.really_delete)} ${category.nama}?")
//                    alertDelete.setButton(AlertDialog.BUTTON_POSITIVE, "Yes") { dialog, _ ->
//                        dialog.dismiss()
//                        when {
//                            category.isDefault -> {
//                                val dialogDefault=AlertDialog.Builder(h.itemView.context).create()
//                                dialogDefault.setMessage(h.itemView.context.getString(R.string.default_cant_delete))
//                                dialogDefault.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok"){dial, _ ->
//                                    dial.dismiss()
//                                }
//                                dialogDefault.show()
//                            }
//                            total == 0 -> categorViewmodel.delete(category)
//                            else -> Toast.makeText(activity, activity.getString(R.string.cant_delete_category_with_data), Toast.LENGTH_SHORT).show()
//                        }
//                    }
//                    alertDelete.setButton(AlertDialog.BUTTON_NEGATIVE, "No") { dialog, _ ->
//                        dialog.dismiss()
//                    }
//                    alertDelete.show()
//                }
//                alert.show()
//            }
        }

        fun setData(datas: List<Category>) {
            this.data = datas
            notifyDataSetChanged()
        }
    }
}