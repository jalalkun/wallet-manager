package xyz.jalalkun.walletmanager.menu

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatDelegate
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity
import xyz.jalalkun.walletmanager.MainActivity
import xyz.jalalkun.walletmanager.R
import xyz.jalalkun.walletmanager.base.BaseActivity
import xyz.jalalkun.walletmanager.databinding.ActivitySettingsBinding
import xyz.jalalkun.walletmanager.databinding.PopupPengaturanThemeBinding
import xyz.jalalkun.walletmanager.util.Cons
import xyz.jalalkun.walletmanager.util.LogTest
import xyz.jalalkun.walletmanager.util.Sessions

class SettingsActivity : BaseActivity() {
    private val TAG : String = "SettingsActivity"
    private lateinit var binding: ActivitySettingsBinding
    private var tema = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        sessions = Sessions.getInstance(this)!!
        setclick()
    }

    private fun setclick(){
        binding.lnTema.onClick { popUpTema() }
        binding.back.onClick {
            finish()
            startActivity<MainActivity>()
        }
    }

    override fun onResume() {
        super.onResume()
        LogTest().e(TAG, "onresume")
        tema = sessions.getIntValue(Cons.SIMPAN_TEMA)
        when(tema){
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM->binding.temaDipilih.text = (getString(R.string.tema_sesuai_sistem))
            AppCompatDelegate.MODE_NIGHT_NO->binding.temaDipilih.text = (getString(R.string.tema_terang))
            AppCompatDelegate.MODE_NIGHT_YES->binding.temaDipilih.text = (getString(R.string.tema_gelap))
        }
    }

    private fun popUpTema(){
        val temaPopup = PopupPengaturanThemeBinding.inflate(LayoutInflater.from(this))
        val builder = AlertDialog.Builder(this)
        builder.setView(temaPopup.root)
        val dialog = builder.create()
        dialog.show()
        when(tema){
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM->temaPopup.radioTema.check(temaPopup.rbTemaSistem.id)
            AppCompatDelegate.MODE_NIGHT_NO->temaPopup.radioTema.check(temaPopup.rbTemaCerah.id)
            AppCompatDelegate.MODE_NIGHT_YES->temaPopup.radioTema.check(temaPopup.rbTemaGelap.id)
        }
        temaPopup.btnSimpan.onClick {
            when(temaPopup.radioTema.checkedRadioButtonId){
                temaPopup.rbTemaCerah.id->setTema(AppCompatDelegate.MODE_NIGHT_NO)
                temaPopup.rbTemaGelap.id->setTema(AppCompatDelegate.MODE_NIGHT_YES)
                temaPopup.rbTemaSistem.id->setTema(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
            }
            this@SettingsActivity.onResume()
            dialog.dismiss()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
        startActivity<MainActivity>()
    }

    private fun setTema(tema: Int){
        sessions.setIntValue(Cons.SIMPAN_TEMA, tema)
        AppCompatDelegate.setDefaultNightMode(tema)
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}