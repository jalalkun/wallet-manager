package xyz.jalalkun.walletmanager.menu

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import com.google.firebase.database.FirebaseDatabase
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast
import xyz.jalalkun.walletmanager.base.BaseActivity
import xyz.jalalkun.walletmanager.databinding.ActivityCriticBinding

class CriticActivity : BaseActivity() {
    private lateinit var binding: ActivityCriticBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCriticBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        binding.back.onClick { finish() }
        binding.btnBatal.onClick { finish() }
        binding.btnKirim.onClick {
            val database = FirebaseDatabase.getInstance()
            val myRef = database.getReference("walletmanager-kritiksaran")

            val reqString = (Build.MANUFACTURER
                    + " " + Build.MODEL + " " + Build.VERSION.RELEASE
                    + " " + Build.VERSION_CODES::class.java.fields[Build.VERSION.SDK_INT].name)
            val kritikSaran = KritikSaran(
                reqString,
                binding.kritikSaranInput.text.toString()
            )
            myRef.setValue(kritikSaran)
            toast("Terimakasih atas kritik dan saran nya")
            finish()
        }
    }

    data class KritikSaran(
        var phone: String,
        var kritikSaran: String
    )
}