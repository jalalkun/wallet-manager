package xyz.jalalkun.walletmanager.util

import java.text.NumberFormat

class FormatMoney {
    fun rp(num: Int) : String{
        return NumberFormat.getInstance().format(num);
    }

    fun rp(num: Long) : String{
        return NumberFormat.getInstance().format(num);
    }

    fun noRp(num: Int) : String{
        return NumberFormat.getInstance().format(num)
    }

    fun noRp(num: Long) : String{
        return NumberFormat.getInstance().format(num)
    }
}