package xyz.jalalkun.walletmanager.util

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface

class Alert {
    fun yesNo(context: Context, message: String, actionListener: DialogInterface.OnClickListener){
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
        builder.setNegativeButton(android.R.string.no) { dialog, which -> dialog.dismiss() }
        builder.setPositiveButton(android.R.string.yes, actionListener)
        builder.show()
    }
}