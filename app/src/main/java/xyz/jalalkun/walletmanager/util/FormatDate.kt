package xyz.jalalkun.walletmanager.util

import java.text.SimpleDateFormat
import java.util.*

class FormatDate {
    fun getToday():String{
        val month = arrayOf(
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        )
        val bulan: String
        val change: String
        val dateformate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())
        val list = dateformate.split("-").toTypedArray()
        bulan = month[list[1].toInt() - 1]
        change = list[0] + " " + bulan + " " + list[2]
        return change
    }
}