package xyz.jalalkun.walletmanager.util

object Cons {
    val DB_NAME = "wallet_manager_db"
    val IN = 1
    val OUT = 0
    val SIMPAN_TEMA = "simpan_tema"
    val PAGE_DIPILIH_MAIN = "page_dipilih_di_main"
}