package xyz.jalalkun.walletmanager.util

import android.util.Log
import xyz.jalalkun.walletmanager.BuildConfig

class LogTest {
    fun e(TAG : String, data : String){
        if (BuildConfig.DEBUG)Log.e(TAG, "error log $data")
    }
}