package xyz.jalalkun.walletmanager.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class Sessions {
    companion object {
        var myObj: Sessions? = null
        lateinit var myPrefs: SharedPreferences
        lateinit var prefsEditor: SharedPreferences.Editor

        fun getInstance(ctx: Context?): Sessions? {
            if (myObj == null) {
                myObj = Sessions()
                myPrefs = PreferenceManager.getDefaultSharedPreferences(ctx)
                prefsEditor = myPrefs.edit()
            }

            return myObj
        }
    }

    fun clearAllSessions() {
        prefsEditor = myPrefs.edit()
        prefsEditor.clear()
        prefsEditor.commit()
    }

    fun clearSessions(key: String) {
        prefsEditor.remove(key)
        prefsEditor.commit()
    }

    fun setIntValue(tag: String, value: Int) {
        prefsEditor.putInt(tag, value)
        prefsEditor.apply()
    }

    fun getIntValue(tag: String) = myPrefs.getInt(tag, 0)
}