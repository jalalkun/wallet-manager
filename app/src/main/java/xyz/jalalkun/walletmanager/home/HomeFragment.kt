package xyz.jalalkun.walletmanager.home

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import org.jetbrains.anko.image
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onScrollChange
import org.jetbrains.anko.textColor
import xyz.jalalkun.walletmanager.MainActivity
import xyz.jalalkun.walletmanager.R
import xyz.jalalkun.walletmanager.base.BaseFragment
import xyz.jalalkun.walletmanager.databinding.AdapterHomeLogKeuanganBinding
import xyz.jalalkun.walletmanager.databinding.HomePageBinding
import xyz.jalalkun.walletmanager.db.log.LogKeuangan
import xyz.jalalkun.walletmanager.db.log.LogKeuanganViewmodel
import xyz.jalalkun.walletmanager.db.category.CategoryiewModel
import xyz.jalalkun.walletmanager.util.Cons
import xyz.jalalkun.walletmanager.util.FormatDate
import xyz.jalalkun.walletmanager.util.FormatMoney
import xyz.jalalkun.walletmanager.util.LogTest
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : BaseFragment() {
    private val TAG = "HomeFragment"
    private lateinit var binding: HomePageBinding
    private lateinit var midnight: Date
    private lateinit var logKeuanganViewmodel: LogKeuanganViewmodel

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomePageBinding.inflate(LayoutInflater.from(context), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.hariIni.text = (FormatDate().getToday())
        MobileAds.initialize(requireActivity()) {}
        binding.adView.loadAd(AdRequest.Builder().build())

        logKeuanganViewmodel =
            run { ViewModelProvider(this@HomeFragment)[LogKeuanganViewmodel::class.java] }

        val adapter =
            AdapterLogKeuangan(
                requireContext(),
                this,
                this
            )
        binding.rvLogKeuangan.layoutManager = LinearLayoutManager(requireContext())
        binding.rvLogKeuangan.adapter = adapter

        logKeuanganViewmodel.filterLog.observe(viewLifecycleOwner, Observer {
            LogTest().e(TAG, "all log ${Gson().toJson(it)}")
            if (it.isEmpty()) {
                binding.rvLogKeuangan.visibility = View.GONE
                binding.cardBelumAdaLog.visibility = View.VISIBLE
            } else {
                binding.rvLogKeuangan.visibility = View.VISIBLE
                binding.cardBelumAdaLog.visibility = View.GONE
            }
            adapter.setLogs(it)
            setTotal(it)
        })

        val c: Calendar = GregorianCalendar()
        c.set(Calendar.HOUR_OF_DAY, 0) //anything 0 - 23
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        midnight = c.time

        binding.nestedScroll.scrollTo(0, 0)

        binding.nestedScroll.onScrollChange { _, _, scrollY, _, oldScrollY ->
            if (scrollY > oldScrollY) binding.fabLogKeuangan.hide()
            if (oldScrollY > scrollY) binding.fabLogKeuangan.show()
        }

        setClick()
    }

    private fun setClick() {
        binding.menu.onClick {
            (context as MainActivity).openRightSideMenu()
        }
        binding.fabLogKeuangan.onClick {
            requireActivity().startActivity<PopupAddLogActivity>()
        }
    }

    private fun setTotal(data: List<LogKeuangan>) {
        var totalIn: Long = 0
        var totalOut: Long = 0
        for (log in data) {
            if (log.inOut == Cons.IN) {
                totalIn += log.total
            } else {
                totalOut += log.total
            }
        }

        if (totalIn < totalOut) {
            binding.totalInOut.text = FormatMoney().rp(totalOut - totalIn)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                binding.totalInOut.textColor = requireContext().getColor(R.color.red_out)
            } else {
                binding.totalInOut.textColor = resources.getColor(R.color.red_out)
            }
        } else {
            binding.totalInOut.text = FormatMoney().rp(totalIn - totalOut)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                binding.totalInOut.textColor = requireContext().getColor(R.color.green_in)
            } else {
                binding.totalInOut.textColor = resources.getColor(R.color.green_in)
            }
        }

        binding.totalIn.text = FormatMoney().rp(totalIn)
        binding.totalOut.text = FormatMoney().rp(totalOut)
    }

    override fun onResume() {
        super.onResume()
        LogTest().e(TAG, "onresume")
        logKeuanganViewmodel.getByDate(this, midnight.time, System.currentTimeMillis())
    }


    class AdapterLogKeuangan(
        private val context: Context,
        private val viewModelStoreOwner: ViewModelStoreOwner,
        private val lifecycleOwner: LifecycleOwner
    ) :
        RecyclerView.Adapter<AdapterLogKeuangan.Holder>() {
        private lateinit var binding: AdapterHomeLogKeuanganBinding
        private var logs = emptyList<LogKeuangan>()
        private lateinit var categoryiewModel: CategoryiewModel

        class Holder(v: AdapterHomeLogKeuanganBinding) : RecyclerView.ViewHolder(v.root) {
            var judul = v.judul
            var ivUpDown = v.ivUpDown
            var cvRoot = v.cvRoot
            var tanggalJam = v.tanggalJam
            var detail = v.detail
            var total = v.total
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            binding =
                AdapterHomeLogKeuanganBinding.inflate(LayoutInflater.from(context), parent, false)
            categoryiewModel =
                parent.context.run { ViewModelProvider(viewModelStoreOwner)[CategoryiewModel::class.java] }
            return Holder(
                binding
            )
        }

        override fun getItemCount(): Int {
            return logs.size
        }

        override fun onBindViewHolder(h: Holder, position: Int) {
            val log = logs[position]
            val formatedDate: String = SimpleDateFormat("dd-MM-yyyy").format(log.createdAt)
            h.judul.text = (log.judul)
            h.detail.text = log.detail
            h.tanggalJam.text = (formatedDate)
            h.total.text = (FormatMoney().rp(log.total))
            if (log.inOut == Cons.IN) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    h.cvRoot.strokeColor = h.itemView.context.getColor(R.color.green_in)
                    h.ivUpDown.image =
                        h.itemView.context.getDrawable(R.drawable.ic_up_green)
                } else {
                    h.cvRoot.strokeColor = h.itemView.context.resources.getColor(R.color.green_in)
                    h.ivUpDown.image =
                        h.itemView.resources.getDrawable(R.drawable.ic_up_green)
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    h.cvRoot.strokeColor = h.itemView.context.getColor(R.color.red_out)
                    h.ivUpDown.image =
                        h.itemView.context.getDrawable(R.drawable.ic_down_red)
                } else {
                    h.cvRoot.strokeColor = h.itemView.context.resources.getColor(R.color.red_out)
                    h.ivUpDown.image =
                        h.itemView.resources.getDrawable(R.drawable.ic_down_red)
                }
            }
            h.itemView.onClick {
                h.itemView.context.startActivity<PopupAddLogActivity>(
                    "isEdit" to true,
                    "idLog" to log.id
                )
            }
        }

        internal fun setLogs(logs: List<LogKeuangan>) {
            this.logs = logs
            notifyDataSetChanged()
        }
    }
}