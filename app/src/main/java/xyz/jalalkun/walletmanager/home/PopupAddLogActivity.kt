package xyz.jalalkun.walletmanager.home

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.animation.DecelerateInterpolator
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast
import xyz.jalalkun.walletmanager.R
import xyz.jalalkun.walletmanager.base.BaseActivity
import xyz.jalalkun.walletmanager.databinding.ActivityPopupAddLogBinding
import xyz.jalalkun.walletmanager.db.log.LogKeuangan
import xyz.jalalkun.walletmanager.db.log.LogKeuanganViewmodel
import xyz.jalalkun.walletmanager.db.category.Category
import xyz.jalalkun.walletmanager.db.category.CategoryiewModel
import xyz.jalalkun.walletmanager.util.Cons
import xyz.jalalkun.walletmanager.util.ID
import xyz.jalalkun.walletmanager.util.LogTest

class PopupAddLogActivity : BaseActivity() {
    private lateinit var binding: ActivityPopupAddLogBinding
    private var darkStatusBar = false
    private val tag = "PopupAddLogActivity"
    private var types = emptyList<Category>()
    private var nameTypes = arrayListOf<String>()
    private lateinit var categoryDipilih: Category
    private lateinit var categoryiewModel: CategoryiewModel
    private lateinit var logKeuanganViewmodel: LogKeuanganViewmodel
    private var checkId = 0
    private var isEdit = false
    private var idLog = ""
    private lateinit var logDiedit: LogKeuangan
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(0, 0)
        binding = ActivityPopupAddLogBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        MobileAds.initialize(this) {}
        binding.adView.loadAd(AdRequest.Builder().build())
        setPopUp()
        init()
    }

    private fun init() {
        logKeuanganViewmodel = run {
            ViewModelProvider(this)[LogKeuanganViewmodel::class.java]
        }
        isEdit = intent.getBooleanExtra("isEdit", false)
        categoryiewModel = run {
            ViewModelProvider(this)[CategoryiewModel::class.java]
        }
        categoryiewModel.filterType.observe(this, Observer {
            types = it
            LogTest().e(tag, Gson().toJson(types))
            nameTypes.clear()
            for (type in types) {
                nameTypes.add(type.nama)
            }
            LogTest().e(tag, "type ${Gson().toJson(it)}")
            setTypes()
        })
        binding.toggleButton.isSingleSelection = true
        binding.toggleButton.addOnButtonCheckedListener { _, checkedId, isChecked ->
            LogTest().e(tag, " $checkedId $isChecked")
            if (isChecked) {
                checkId = checkedId
                when (checkedId) {
                    binding.togglePemasukan.id -> {
                        if (!isChecked) binding.togglePemasukan.isChecked = true
                        binding.togglePengeluaran.isChecked = false
                        filterType(Cons.IN)
                        LogTest().e(tag, "pemasukan")
                    }
                    binding.togglePengeluaran.id -> {
                        if (!isChecked) binding.togglePengeluaran.isChecked = true
                        binding.togglePemasukan.isChecked = false
                        filterType(Cons.OUT)
                        LogTest().e(tag, "pengeluaran")
                    }
                }
            } else {
                if (checkId == checkedId) binding.toggleButton.check(checkId)
                when (checkedId) {
                    binding.togglePemasukan.id -> LogTest().e(tag, "unCheck pemasukan")
                    binding.togglePengeluaran.id -> LogTest().e(tag, "uncheck pengeluaran")
                }
            }
        }
        binding.togglePengeluaran.isChecked = true
        binding.btnBatal.onClick { finish() }
        binding.btnSimpan.onClick {
            if (isEdit) {
                if (validateData()) {
                    val sysTime = System.currentTimeMillis()
                    val logDiupdate = LogKeuangan(
                        id = logDiedit.id,
                        judul = categoryDipilih.nama,
                        detail = binding.detailInput.editText?.text.toString(),
                        total = binding.nominal.text.toString().toLong(),
                        type = categoryDipilih.id,
                        createdAt = logDiedit.createdAt,
                        modifiedAt = sysTime,
                        inOut = categoryDipilih.inOut
                        ,wallet = ""
                    )
                    logKeuanganViewmodel.update(logDiupdate)
                    finish()
                }
            } else {
                if (validateData()) {
                    val sysTime = System.currentTimeMillis()
                    val logAdd = LogKeuangan(
                        ID().make(),
                        categoryDipilih.nama,
                        binding.detailInput.editText?.text.toString(),
                        binding.nominal.text.toString().toLong(),
                        categoryDipilih.id,
                        sysTime,
                        sysTime,
                        categoryDipilih.inOut
                        ,""
                    )
                    logKeuanganViewmodel.insert(logAdd)
                    finish()
                }
            }
        }
        binding.spinnerType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                categoryDipilih = types[position]
                LogTest().e(tag, "type dipili ${types[position].nama}")
            }
        }

        if (isEdit) {
            idLog = intent.getStringExtra("idLog")!!
            logKeuanganViewmodel.oneLog.observe(this, Observer {
                LogTest().e(tag, "log ${Gson().toJson(it)}")
                logDiedit = it
                when (it.inOut) {
                    Cons.IN -> binding.togglePemasukan.isChecked = true
                    Cons.OUT -> binding.togglePengeluaran.isChecked = true
                }
                binding.detailInput.editText!!.setText(it.detail)
                binding.nominal.setText("${it.total}")
                LogTest().e(tag, "after set total")
                LogTest().e(tag, "after delay")
                var pos: Int = -1
                for (type in types) {
                    pos += 1
                    if (type.id == it.type) {
                        binding.spinnerType.setSelection(pos)
                    }
                }
            })
            loadLog(idLog)
        }
    }

    private fun loadLog(id: String) {
        logKeuanganViewmodel.getOneData(this, id)
    }

    private fun validateData(): Boolean {
        if (binding.nominal.text.toString().isEmpty()) {
            toast("Masukan nominal")
            return false
        }
        return true
    }

    private fun setTypes() {
        val adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, nameTypes)
        binding.spinnerType.adapter = adapter
    }

    private fun filterType(jenis: Int) {
        categoryiewModel.getFilterType(this, jenis)
    }

    private fun setPopUp() {
        // Set the Status bar appearance for different API levels
        if (Build.VERSION.SDK_INT in 19..20) {
            setWindowFlag(this, true)
        }
        if (Build.VERSION.SDK_INT >= 21) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // If you want dark status bar, set darkStatusBar to true
                if (darkStatusBar) {
                    this.window.decorView.systemUiVisibility =
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                }
                this.window.statusBarColor = Color.TRANSPARENT
                setWindowFlag(this, false)
            }
        }

        // Fade animation for the background of Popup Window
        val alpha = 100 //between 0-255
        val alphaColor = ColorUtils.setAlphaComponent(Color.parseColor("#000000"), alpha)
        val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), Color.TRANSPARENT, alphaColor)
        colorAnimation.duration = 500 // milliseconds
        colorAnimation.addUpdateListener { animator ->
            binding.popupWindowBackground.setBackgroundColor(animator.animatedValue as Int)
        }
        colorAnimation.start()

        // Fade animation for the Popup Window
        binding.popupWindowViewWithBorder.alpha = 0f
        binding.popupWindowViewWithBorder.animate().alpha(1f).setDuration(500).setInterpolator(
            DecelerateInterpolator()
        ).start()
    }

    private fun setWindowFlag(activity: Activity, on: Boolean) {
        val win = activity.window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
        } else {
            winParams.flags =
                winParams.flags and WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS.inv()
        }
        win.attributes = winParams
    }

    override fun onBackPressed() {
        // Fade animation for the background of Popup Window when you press the back button
        val alpha = 100 // between 0-255
        val alphaColor = ColorUtils.setAlphaComponent(Color.parseColor("#000000"), alpha)
        val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), alphaColor, Color.TRANSPARENT)
        colorAnimation.duration = 500 // milliseconds
        colorAnimation.addUpdateListener { animator ->
            binding.popupWindowBackground.setBackgroundColor(
                animator.animatedValue as Int
            )
        }

        // Fade animation for the Popup Window when you press the back button
        binding.popupWindowViewWithBorder.animate().alpha(0f).setDuration(500).setInterpolator(
            DecelerateInterpolator()
        ).start()

        // After animation finish, close the Activity
        colorAnimation.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                finish()
                overridePendingTransition(0, 0)
            }
        })
        colorAnimation.start()
    }
}