package xyz.jalalkun.walletmanager.history

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import org.jetbrains.anko.image
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.textColor
import xyz.jalalkun.walletmanager.MainActivity
import xyz.jalalkun.walletmanager.R
import xyz.jalalkun.walletmanager.base.BaseFragment
import xyz.jalalkun.walletmanager.databinding.AdapterHomeLogKeuanganBinding
import xyz.jalalkun.walletmanager.databinding.HistoryPageBinding
import xyz.jalalkun.walletmanager.db.log.LogKeuangan
import xyz.jalalkun.walletmanager.db.log.LogKeuanganViewmodel
import xyz.jalalkun.walletmanager.db.category.CategoryiewModel
import xyz.jalalkun.walletmanager.util.Cons
import xyz.jalalkun.walletmanager.util.FormatMoney
import xyz.jalalkun.walletmanager.util.LogTest
import java.text.SimpleDateFormat
import java.util.*

class HistoryPage : BaseFragment() {
    private lateinit var binding: HistoryPageBinding
    private val TAG = "RiwayatPage"

    private var isKabisat = false
    private lateinit var logKeuanganViewModel: LogKeuanganViewmodel
    private var lihat = false

    companion object {
        fun newInstance() = HistoryPage()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HistoryPageBinding.inflate(LayoutInflater.from(context), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logKeuanganViewModel = run { ViewModelProvider(this)[LogKeuanganViewmodel::class.java] }
        setDataRespon()
        MobileAds.initialize(requireActivity()) {}
        binding.adView.loadAd(AdRequest.Builder().build())
        val year = Calendar.getInstance()
        val tahun = year.get(Calendar.YEAR)
        val bulan = year.get(Calendar.MONTH)
        if (tahun.rem(4) == 0) isKabisat = true
        val cFrom = GregorianCalendar()
        val cTo = GregorianCalendar()
        LogTest().e(TAG, "year ${year.get(Calendar.YEAR)} kabisat $isKabisat")
        cFrom.set(Calendar.DAY_OF_MONTH, 1)
        cFrom.set(Calendar.HOUR_OF_DAY, 0)
        cFrom.set(Calendar.MINUTE, 0)
        cFrom.set(Calendar.SECOND, 0)
        cTo.set(Calendar.DAY_OF_MONTH, 31)
        cTo.set(Calendar.HOUR_OF_DAY, 23)
        cTo.set(Calendar.MINUTE, 59)
        cTo.set(Calendar.SECOND, 59)
        binding.tabs.setupWithViewPager(ViewPager(requireContext()))
        binding.tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                LogTest().e(TAG, "onTabReselected")
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                LogTest().e(TAG, "onTabUnselected")
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                LogTest().e(TAG, "onTabSelected ${tab?.position}")
                when (tab?.position) {
                    0 -> {
                        cFrom.set(Calendar.MONTH, Calendar.JANUARY)
                        cTo.set(Calendar.MONTH, Calendar.JANUARY)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    1 -> {
                        cFrom.set(Calendar.MONTH, Calendar.FEBRUARY)
                        if (isKabisat) cTo.set(Calendar.DAY_OF_MONTH, 29)
                        else cTo.set(Calendar.DAY_OF_MONTH, 28)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    2 -> {
                        cFrom.set(Calendar.MONTH, Calendar.MARCH)
                        cTo.set(Calendar.MONTH, Calendar.MARCH)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    3 -> {
                        cFrom.set(Calendar.MONTH, Calendar.APRIL)
                        cTo.set(Calendar.MONTH, Calendar.APRIL)
                        cTo.set(Calendar.DAY_OF_MONTH, 30)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    4 -> {
                        cFrom.set(Calendar.MONTH, Calendar.MAY)
                        cTo.set(Calendar.MONTH, Calendar.MAY)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    5 -> {
                        cFrom.set(Calendar.MONTH, Calendar.JUNE)
                        cTo.set(Calendar.MONTH, Calendar.JUNE)
                        cTo.set(Calendar.DAY_OF_MONTH, 30)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    6 -> {
                        cFrom.set(Calendar.MONTH, Calendar.JULY)
                        cTo.set(Calendar.MONTH, Calendar.JULY)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    7 -> {
                        cFrom.set(Calendar.MONTH, Calendar.AUGUST)
                        cTo.set(Calendar.MONTH, Calendar.AUGUST)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    8 -> {
                        cFrom.set(Calendar.MONTH, Calendar.SEPTEMBER)
                        cTo.set(Calendar.MONTH, Calendar.SEPTEMBER)
                        cTo.set(Calendar.DAY_OF_MONTH, 30)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    9 -> {
                        cFrom.set(Calendar.MONTH, Calendar.OCTOBER)
                        cTo.set(Calendar.MONTH, Calendar.OCTOBER)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    10 -> {
                        cFrom.set(Calendar.MONTH, Calendar.NOVEMBER)
                        cTo.set(Calendar.MONTH, Calendar.NOVEMBER)
                        cTo.set(Calendar.DAY_OF_MONTH, 30)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                    11 -> {
                        cFrom.set(Calendar.MONTH, Calendar.DECEMBER)
                        cTo.set(Calendar.MONTH, Calendar.DECEMBER)
                        val from = cFrom.timeInMillis
                        val to = cTo.timeInMillis
                        getData(from, to)
                    }
                }
            }
        })
        val b = binding.tabs.getTabAt(bulan)
        when (bulan) {
            0 -> binding.tabs.selectTab(b)
            1 -> binding.tabs.selectTab(b)
            2 -> binding.tabs.selectTab(b)
            3 -> binding.tabs.selectTab(b)
            4 -> binding.tabs.selectTab(b)
            5 -> binding.tabs.selectTab(b)
            6 -> binding.tabs.selectTab(b)
            7 -> binding.tabs.selectTab(b)
            8 -> binding.tabs.selectTab(b)
            9 -> binding.tabs.selectTab(b)
            10 -> binding.tabs.selectTab(b)
            11 -> binding.tabs.selectTab(b)
        }
        clickLihat()
        setClick()
    }

    private fun setClick() {
        binding.menu.onClick {
            (context as MainActivity).openRightSideMenu()
        }
    }

    private fun clickLihat() {
        binding.bukaTutupRiwayat.onClick {
            if (lihat) {
                lihat = false
                binding.rvLogKeuangan.visibility = View.GONE
                binding.ivLihatRiwayat.image =
                    requireActivity().getDrawable(R.drawable.ic_arrow_down_blue)
            } else {
                lihat = true
                binding.rvLogKeuangan.visibility = View.VISIBLE
                binding.ivLihatRiwayat.image =
                    requireActivity().getDrawable(R.drawable.ic_arrow_up_blue)
            }
        }
    }

    private fun setDataRespon() {
        val adapter =
            AdapterLogKeuangan(
                requireContext(),
                this,
                this
            )
        binding.rvLogKeuangan.layoutManager = LinearLayoutManager(requireContext())
        binding.rvLogKeuangan.adapter = adapter
        logKeuanganViewModel.filterLog.observe(viewLifecycleOwner, Observer {
            LogTest().e(TAG, "all log ${Gson().toJson(it)}")
            adapter.setLogs(it)
            setTotal(it)
        })
    }

    private fun setTotal(data: List<LogKeuangan>) {
        var num = 0L
        var numOut = 0L
        for (log in data) {
            if (log.inOut == Cons.IN) num += log.total else numOut += log.total
        }

        if (num < numOut) {
            binding.totalInOut.text = FormatMoney().rp(numOut - num)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                binding.totalInOut.textColor = requireContext().getColor(R.color.red_out)
            } else binding.totalInOut.textColor = resources.getColor(R.color.red_out)
        } else {
            binding.totalInOut.text = FormatMoney().rp(num - numOut)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                binding.totalInOut.textColor = requireContext().getColor(R.color.green_in)
            } else {
                binding.totalInOut.textColor = resources.getColor(R.color.green_in)
            }
        }

        binding.totalIn.text = FormatMoney().rp(num)
        binding.totalOut.text = FormatMoney().rp(numOut)
    }

    fun getData(from: Long, to: Long) {
        logKeuanganViewModel.getByDate(this, from, to)
    }

    class AdapterLogKeuangan(
        val context: Context,
        private val viewModelStoreOwner: ViewModelStoreOwner,
        private val lifecycleOwner: LifecycleOwner
    ) :
        RecyclerView.Adapter<AdapterLogKeuangan.Holder>() {
        private lateinit var binding: AdapterHomeLogKeuanganBinding
        private var logs = emptyList<LogKeuangan>()
        private lateinit var categoryiewModel: CategoryiewModel

        class Holder(v: AdapterHomeLogKeuanganBinding) : RecyclerView.ViewHolder(v.root) {
            var judul = v.judul
            var ivUpDown = v.ivUpDown
            var cvRoot = v.cvRoot
            var tanggalJam = v.tanggalJam
            var detail = v.detail
            var total = v.total
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            binding =
                AdapterHomeLogKeuanganBinding.inflate(LayoutInflater.from(context), parent, false)
            this.categoryiewModel =
                parent.context.run { ViewModelProvider(viewModelStoreOwner)[CategoryiewModel::class.java] }
            return Holder(
                binding
            )
        }

        override fun getItemCount(): Int {
            return logs.size
        }

        override fun onBindViewHolder(h: Holder, position: Int) {
            val log = logs[position]
            val formatedDate: String = SimpleDateFormat("dd-MM-yyyy").format(log.createdAt)
            h.judul.text = (log.judul)
            h.detail.text = log.detail
            h.tanggalJam.text = (formatedDate)
            h.total.text = (FormatMoney().rp(log.total))
            categoryiewModel.type.observe(lifecycleOwner, Observer {
                if (it.id == log.type) {
                    if (it.inOut == Cons.IN) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            h.cvRoot.strokeColor = h.itemView.context.getColor(R.color.green_in)
                            h.ivUpDown.image =
                                h.itemView.context.getDrawable(R.drawable.ic_up_green)
                        } else {
                            h.cvRoot.strokeColor =
                                h.itemView.context.resources.getColor(R.color.green_in)
                            h.ivUpDown.image =
                                h.itemView.resources.getDrawable(R.drawable.ic_up_green)
                        }
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            h.cvRoot.strokeColor = h.itemView.context.getColor(R.color.red_out)
                            h.ivUpDown.image =
                                h.itemView.context.getDrawable(R.drawable.ic_down_red)
                        } else {
                            h.cvRoot.strokeColor =
                                h.itemView.context.resources.getColor(R.color.red_out)
                            h.ivUpDown.image =
                                h.itemView.resources.getDrawable(R.drawable.ic_down_red)
                        }
                    }
                }
            })
            categoryiewModel.getTypeById(lifecycleOwner, log.type)
        }

        internal fun setLogs(logs: List<LogKeuangan>) {
            this.logs = logs
            notifyDataSetChanged()
        }
    }
}