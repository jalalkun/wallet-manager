package xyz.jalalkun.walletmanager

import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import org.jetbrains.anko.startActivity
import xyz.jalalkun.walletmanager.base.BaseActivity
import xyz.jalalkun.walletmanager.databinding.ActivityMainBinding
import xyz.jalalkun.walletmanager.db.category.CategoryiewModel
import xyz.jalalkun.walletmanager.history.HistoryPage
import xyz.jalalkun.walletmanager.home.HomeFragment
import xyz.jalalkun.walletmanager.menu.AboutActivity
import xyz.jalalkun.walletmanager.menu.CategoryActivity
import xyz.jalalkun.walletmanager.menu.CriticActivity
import xyz.jalalkun.walletmanager.menu.SettingsActivity
import xyz.jalalkun.walletmanager.util.Cons.PAGE_DIPILIH_MAIN
import xyz.jalalkun.walletmanager.util.LogTest

class MainActivity : BaseActivity() {
    private val tag = "MainActivity"
    private lateinit var binding: ActivityMainBinding
    private lateinit var categoryiewModel: CategoryiewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        sessions.setIntValue(PAGE_DIPILIH_MAIN, 0)
        categoryiewModel = ViewModelProvider(this)[CategoryiewModel::class.java]
        LogTest().e(tag, "oncreate")

        categoryiewModel.allCategory.observe(this, Observer {
            LogTest().e(tag, "type ${Gson().toJson(it)}")
        })

        setClick()
    }

    private fun setClick(){
        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when(item.itemId){
                R.id.home_page -> {
                    sessions.setIntValue(PAGE_DIPILIH_MAIN, 0)
                    setPage()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.history_page -> {
                    sessions.setIntValue(PAGE_DIPILIH_MAIN, 1)
                    setPage()
                    return@setOnNavigationItemSelectedListener true
                }

                else -> return@setOnNavigationItemSelectedListener false

            }
        }

        binding.rightSideMenu.setNavigationItemSelectedListener { item ->
            when(item.itemId){
                R.id.menu_settings -> {
                    binding.drawwerLayout.closeDrawers()
                    startActivity<SettingsActivity>()
                    this.finish()
                    return@setNavigationItemSelectedListener true
                }

                R.id.menu_critic -> {
                    binding.drawwerLayout.closeDrawers()
                    startActivity<CriticActivity>()
                    return@setNavigationItemSelectedListener true
                }

                R.id.menu_about -> {
                    binding.drawwerLayout.closeDrawers()
                    startActivity<AboutActivity>()
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_category -> {
                    binding.drawwerLayout.closeDrawers()
                    startActivity<CategoryActivity>()
                    return@setNavigationItemSelectedListener true
                }

                else -> return@setNavigationItemSelectedListener false
            }
        }
    }

    private fun setPage(){
        when (sessions.getIntValue(PAGE_DIPILIH_MAIN)){
            0 -> changePaga(HomeFragment.newInstance())
            1 -> changePaga(HistoryPage.newInstance())
        }
    }

    private fun changePaga(fragment: Fragment){
        supportFragmentManager
            .beginTransaction()
            .replace(binding.frame.id, fragment)
            .commit()
    }

    fun openRightSideMenu() {
        if (!binding.drawwerLayout.isDrawerOpen(GravityCompat.END)) {
            binding.drawwerLayout.openDrawer(GravityCompat.END, true)
        } else binding.drawwerLayout.closeDrawer(GravityCompat.END, true)
    }

    override fun onResume() {
        super.onResume()
        LogTest().e(tag, "onresume")
        setPage()
    }
}