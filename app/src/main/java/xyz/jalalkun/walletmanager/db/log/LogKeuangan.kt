package xyz.jalalkun.walletmanager.db.log

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "log_keuangan")
data class LogKeuangan (
    @PrimaryKey @ColumnInfo(name = "id") var id : String,
    @ColumnInfo(name = "judul") val judul: String,
    @ColumnInfo(name = "detail") val detail: String,
    @ColumnInfo(name = "total") val total : Long,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "created_at") var createdAt: Long,
    @ColumnInfo(name = "modified_at") var modifiedAt: Long,
    @ColumnInfo(name = "in_out") var inOut : Int,
    @ColumnInfo(name = "wallet")var wallet: String
)