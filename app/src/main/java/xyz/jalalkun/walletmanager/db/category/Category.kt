package xyz.jalalkun.walletmanager.db.category

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import xyz.jalalkun.walletmanager.db.category.Category.Companion.NAME

/*Create Category database*/
/*
* id : berisi id dengan format 3 huruf
* nama : nama dari type
* in_out : berisi value Int 0 = out, 1 = in
* */

@Entity(
    tableName = "type_in_out",
    indices = [Index(value = [NAME], unique = true)]
)
data class Category (
    @PrimaryKey @ColumnInfo(name = "id") val id : String,
    @ColumnInfo(name = NAME)val nama: String,
    @ColumnInfo(name = "inOut")val inOut: Int,
    @ColumnInfo(name = "warna")val warna: String,
    @ColumnInfo(name = "icon")val icon: String,
    @ColumnInfo(name = "isDefault")val isDefault: Boolean
) {
    companion object{
        const val NAME = "nama"
    }
}