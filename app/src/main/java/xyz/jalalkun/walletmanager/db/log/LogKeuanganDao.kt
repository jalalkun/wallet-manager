package xyz.jalalkun.walletmanager.db.log

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface LogKeuanganDao {

    @Query("SELECT * from log_keuangan ORDER by created_at ASC")
    fun getAllLog() : LiveData<List<LogKeuangan>>

    @Query("SELECT * from log_keuangan WHERE created_at BETWEEN :from AND :to")
    fun getByDate(from: Long, to: Long) : LiveData<List<LogKeuangan>>

    @Query("SELECT * from log_keuangan WHERE id == :id")
    fun getOneData(id: String) : LiveData<LogKeuangan>

    @Query("SELECT * from log_keuangan WHERE type == :category")
    fun getByCategory(category: String) : LiveData<List<LogKeuangan>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(logKeuangan: LogKeuangan)

    @Update
    fun update(logKeuangan: LogKeuangan)

}