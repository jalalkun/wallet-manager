package xyz.jalalkun.walletmanager.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import xyz.jalalkun.walletmanager.db.log.LogKeuangan
import xyz.jalalkun.walletmanager.db.log.LogKeuanganDao
import xyz.jalalkun.walletmanager.db.category.Category
import xyz.jalalkun.walletmanager.db.category.CategoryDao
import xyz.jalalkun.walletmanager.util.Cons

@Database(entities = [Category::class, LogKeuangan::class], version = 1)
abstract class DbCreate : RoomDatabase() {
    abstract fun typeDao(): CategoryDao
    abstract fun logDao(): LogKeuanganDao

    companion object {
        @Volatile
        private var INSTANCE: DbCreate? = null

        fun getDatabase(context: Context, scope: CoroutineScope): DbCreate {
            return INSTANCE ?: synchronized(this) {
                val instance: DbCreate =
                    Room.databaseBuilder(context.applicationContext, DbCreate::class.java, Cons.DB_NAME)
                        .fallbackToDestructiveMigration()
//                        .addMigrations(MIGRATION_1_2)
                        .addCallback(DbCreateCallback(scope))
                        .build()
                INSTANCE = instance
                instance
            }
        }

        val MIGRATION_1_2 = object : Migration(1, 2){
            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("""
                    INSERT INTO new_log_keuangan(id, judul, detail, total, type, created_at, modified_at, in_out, wallet)
                    SELECT id, judul, detail, total, type, created_at, modified_at, in_out, wallet FROM log_keuangan
                """.trimIndent())
                database.execSQL("DROP TABLE log_keuangan")
                database.execSQL("ALTER TABLE new_log_keuangan RENAME TO log_keuangan")
            }
        }

        fun createLogTable(database: SupportSQLiteDatabase){
            database.execSQL("""
                    CREATE TABLE log_keuangan (
                        'id' INTEGER PRIMARY KEY NOT NULL,
                        'judul' TEXT,
                        'detail' TEXT,
                        'total' TEXT,
                        'type' TEXT,
                        'created_at' INTEGER,
                        'modified_at' INTEGER,
                        'in_out' INTEGER,
                        'wallet' TEXT NOT NULL DEFAULT ''
                    )
                """.trimIndent())
        }

        private class DbCreateCallback(private val scope: CoroutineScope) :
            RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateType(database.typeDao())
                    }
                }
            }
        }

        fun populateType(categoryDao: CategoryDao) {
            categoryDao.deleteAll()
            /*out*/
            categoryDao.insert(
                Category(
                    "0",
                    "FOOD",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "1",
                    "TRANSPORTATION",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "2",
                    "SNACK",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "3",
                    "COMMUNICATION",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "4",
                    "CHARITY",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "5",
                    "ELECTRONIC",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "6",
                    "HOLIDAY",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "7",
                    "VACATION",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "8",
                    "FURNITURE",
                    0,
                    "FFFFFF",
                    "",
                    true
                )
            )

            /*in*/
            categoryDao.insert(
                Category(
                    "9",
                    "SALARY",
                    1,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "10",
                    "POCKET MONEY",
                    1,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "11",
                    "GIFT",
                    1,
                    "FFFFFF",
                    "",
                    true
                )
            )
            categoryDao.insert(
                Category(
                    "12",
                    "PROFIT",
                    1,
                    "FFFFFF",
                    "",
                    true
                )
            )
        }

    }
}