package xyz.jalalkun.walletmanager.db.category

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import xyz.jalalkun.walletmanager.db.DbCreate

/**
 * View Model to keep a reference to the word repository and
 * an up-to-date list of all type.
 */
class CategoryiewModel(application: Application) : AndroidViewModel(application) {
    private val repository: CategoryRepository
    val allCategory: LiveData<List<Category>>
    var type = MutableLiveData<Category>()
    var filterType= MutableLiveData<List<Category>>()
    var resultInsert = MutableLiveData<Long>()

    init {
        val typeDao = DbCreate.getDatabase(application, viewModelScope).typeDao()
        repository = CategoryRepository(typeDao)
        allCategory = repository.allCategory
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(category: Category) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(category)
    }

    fun update(category: Category) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(category)
    }

    fun delete(category: Category) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(category)
    }

    fun getFilterType(lifecycleOwner: LifecycleOwner,jenis: Int){
        repository.getFilterType(jenis).observe(lifecycleOwner, Observer {
            filterType.value = it
        })
    }
    fun getTypeById(lifecycleOwner: LifecycleOwner, id: String){
        repository.getTypeById(id).observe(lifecycleOwner, Observer {
            type.value = it
        })
    }
}