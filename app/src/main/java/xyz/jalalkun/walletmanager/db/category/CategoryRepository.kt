package xyz.jalalkun.walletmanager.db.category

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class CategoryRepository(private val categoryDao: CategoryDao) {
    val allCategory : LiveData<List<Category>> = categoryDao.getAllType()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(category: Category){
        return categoryDao.insert(category)
    }

    suspend fun update(category: Category){
        return categoryDao.update(category)
    }

    suspend fun delete(category: Category){
        categoryDao.delete(category)
    }

    fun getFilterType(jenis: Int):LiveData<List<Category>> {
        return categoryDao.getType(jenis)
    }

    fun getTypeById(id: String):LiveData<Category>{
        return categoryDao.getType(id)
    }
}