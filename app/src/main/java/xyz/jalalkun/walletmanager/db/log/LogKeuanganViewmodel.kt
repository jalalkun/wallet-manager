package xyz.jalalkun.walletmanager.db.log

import android.app.Application
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import xyz.jalalkun.walletmanager.db.DbCreate

class LogKeuanganViewmodel (application: Application) : AndroidViewModel(application){
    private val repository : LogKeuanganRepository

    val allLog : LiveData<List<LogKeuangan>>
    val filterLog = MutableLiveData<List<LogKeuangan>>()
    val oneLog = MutableLiveData<LogKeuangan>()
    val filterCategory = MutableLiveData<List<LogKeuangan>>()

    init {
        val logKeuanganDao = DbCreate.getDatabase(application, viewModelScope).logDao()
        repository = LogKeuanganRepository(logKeuanganDao)
        allLog = repository.allLogKeuangan
    }

    fun insert(logKeuangan: LogKeuangan) = viewModelScope.launch(Dispatchers.IO){
        repository.insert(logKeuangan)
    }

    fun update(logKeuangan: LogKeuangan) = viewModelScope.launch(Dispatchers.IO){
        repository.update(logKeuangan)
    }

    fun getByDate(lifecycleOwner: LifecycleOwner, from: Long, to: Long) {
        repository.getByDate(from, to).observe(lifecycleOwner, Observer {
            filterLog.value = it
        })
    }

    fun getByCategory(lifecycleOwner: LifecycleOwner, category: String){
        repository.getByCategory(category).observe(lifecycleOwner, Observer {
            filterCategory.value = it
        })
    }

    fun getOneData(lifecycleOwner: LifecycleOwner, id: String){
        repository.getOneData(id).observe(lifecycleOwner, Observer {
            oneLog.value = it
        })
    }
}