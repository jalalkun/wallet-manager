package xyz.jalalkun.walletmanager.db.category

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CategoryDao {
    @Query("SELECT * FROM type_in_out")
    fun getAllType(): LiveData<List<Category>>

    @Insert(onConflict =  OnConflictStrategy.ABORT)
    fun insert(category: Category)

    @Update
    fun update(category: Category)

    @Delete
    fun delete(category: Category)

    @Query("DELETE FROM type_in_out")
    fun deleteAll()

    @Query("SELECT * from type_in_out WHERE id LIKE :id LIMIT 1")
    fun getType(id: String) : LiveData<Category>

    @Query("SELECT * from type_in_out WHERE inOut LIKE :jenis")
    fun getType(jenis : Int) : LiveData<List<Category>>
}