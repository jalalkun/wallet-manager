package xyz.jalalkun.walletmanager.db.log

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class LogKeuanganRepository(val logKeuanganDao: LogKeuanganDao) {
    val allLogKeuangan : LiveData<List<LogKeuangan>> = logKeuanganDao.getAllLog()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(logKeuangan : LogKeuangan){
        logKeuanganDao.insert(logKeuangan)
    }

    suspend fun update(logKeuangan: LogKeuangan){
        logKeuanganDao.update(logKeuangan)
    }

    fun getByDate(from: Long, to: Long):LiveData<List<LogKeuangan>>{
        return logKeuanganDao.getByDate(from, to)
    }

    fun getByCategory(category: String) : LiveData<List<LogKeuangan>>{
        return logKeuanganDao.getByCategory(category)
    }

    fun getOneData(id: String):LiveData<LogKeuangan>{
        return logKeuanganDao.getOneData(id)
    }
}